//package cat.vn.pr.config;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//
//import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
//
//import redis.embedded.RedisServer;
//
//@Configuration
//@EnableRedisHttpSession
//public class RedisConfig {
//
//	
//	private RedisServer redisServer;
//	
//	public RedisConfig(RedisProperties redisProperties) {
//        this.redisServer = new RedisServer(redisProperties.getPort());
//    }
//
//	@PostConstruct
//    public void postConstruct() {
//        redisServer.start();
//    }
// 
//    @PreDestroy
//    public void preDestroy() {
//        redisServer.stop();
//    }
//}
