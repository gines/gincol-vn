package cat.vn.pr.config;

public class Constants {
	
	public static final String INDICE_ISSUE = "0";
	public static final String INDICE_DOCS = "1";
	public static final String NOMBRE_SESSION = "busqueda";
	public static final String CAMPO_DESCRIPCIO = "0";
	public static final String CAMPO_DESCRIPCIO_NOMBRE = "descripcio";
	public static final String CAMPO_TITOL = "1";
	public static final String CAMPO_TITOL_NOMBRE = "titol";
	public static final String CAMPO_TODO = "2";
	public static final String CAMPO_TODO_NOMBRE = "tot";
	
	public static final String CAMPO_DATA = "0";
	public static final String CAMPO_DATA_NOMBRE = "data";
}
