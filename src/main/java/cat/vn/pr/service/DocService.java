package cat.vn.pr.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cat.vn.pr.model.Doc;

public interface DocService {
	public long count();
	public Page<Doc> getDocsByData(String texto, Pageable pageable);

}
