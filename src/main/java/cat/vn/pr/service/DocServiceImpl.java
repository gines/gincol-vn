package cat.vn.pr.service;

import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHitSupport;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.SearchPage;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;

import cat.vn.pr.model.Doc;
import cat.vn.pr.repository.DocRepository;

@Component
public class DocServiceImpl implements DocService {

	private static final Logger logger = LoggerFactory.getLogger(DocServiceImpl.class);

	@Autowired
	DocRepository repository;

//	@Autowired
//	private RestHighLevelClient restHighLevelClient;

	@Autowired
	private ElasticsearchRestTemplate elasticsearchTemplate;

	private String index = "spain_docs";

	public long count() {
		logger.info("START count():");
		return repository.count();
	}

	@Override
	public Page<Doc> getDocsByData(String texto, Pageable pageable) {
//		NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
//				.withQuery(new TermQueryBuilder("attachment.content", texto)).withHighlightFields(
//						new HighlightBuilder.Field("attachment.content").fragmentSize(100).numOfFragments(10))
//				.build();
		
		MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("attachment.content", texto);
		
		FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(matchQueryBuilder)
                .boost(0.1f);
		
		NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
				.withQuery(matchQueryBuilder)
				.withHighlightFields(new HighlightBuilder.Field("attachment.content").fragmentSize(100).numOfFragments(10))
				.withPageable(pageable)
				.build();
		
		SearchHits<Doc> searchHits = elasticsearchTemplate.search(searchQuery, Doc.class, IndexCoordinates.of(index));
		SearchPage<Doc> page = SearchHitSupport.searchPageFor(searchHits, searchQuery.getPageable());
		return (Page<Doc>) SearchHitSupport.unwrapSearchHits(page);

	}

}
