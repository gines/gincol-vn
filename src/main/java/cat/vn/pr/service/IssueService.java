package cat.vn.pr.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cat.vn.pr.model.Issue;

public interface IssueService {
	public long count();
	public Page<Issue> getDocsByDescripcio(String texto, Pageable pageable);
	public Page<Issue> getDocsByTitol(String texto, Pageable pageable);
	public Page<Issue> getDocsByTitolDescripcio(String texto, Pageable pageable);

}
