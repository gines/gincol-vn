package cat.vn.pr.service;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHitSupport;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.SearchPage;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Component;

import cat.vn.pr.model.Issue;
import cat.vn.pr.repository.IssueRepository;

@Component
public class IssueServiceImpl implements IssueService {

	private static final Logger logger = LoggerFactory.getLogger(IssueServiceImpl.class);
	
	@Autowired
	IssueRepository repository;
	
	@Autowired
	private ElasticsearchRestTemplate elasticsearchTemplate;
	
	private String index = "catalan_issue";
	
	public long count() {
		logger.info("START count():");
		return repository.count();
	}

	@Override
	public Page<Issue> getDocsByTitol(String texto, Pageable pageable) {

		//inicio nuevo
		MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("titol", texto);
		FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(matchQueryBuilder)
                .boost(0.1f)
                .maxBoost(1f);		
		//fin nuevo
		
		NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
				//.withQuery(matchQueryBuilder)
				.withQuery(functionScoreQueryBuilder)
				.withHighlightFields(new HighlightBuilder.Field("titol").fragmentSize(50).numOfFragments(10))
				.withPageable(pageable)
				.build();
		
		SearchHits<Issue> searchHits = elasticsearchTemplate.search(searchQuery, Issue.class, IndexCoordinates.of(index));
		SearchPage<Issue> page = SearchHitSupport.searchPageFor(searchHits, searchQuery.getPageable());
		return (Page<Issue>) SearchHitSupport.unwrapSearchHits(page);
	}
	
	@Override
	public Page<Issue> getDocsByDescripcio(String texto, Pageable pageable) {
        
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("descripcio", texto);
        FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(matchQueryBuilder)
                .boost(0.1f);	
        
		NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
				//.withQuery(matchQueryBuilder)
				.withQuery(functionScoreQueryBuilder)
				.withHighlightFields(new HighlightBuilder.Field("descripcio").fragmentSize(200).numOfFragments(10))
				.withPageable(pageable)
				.build();
		
		SearchHits<Issue> searchHits = elasticsearchTemplate.search(searchQuery, Issue.class, IndexCoordinates.of(index));
		SearchPage<Issue> page = SearchHitSupport.searchPageFor(searchHits, searchQuery.getPageable());
		return (Page<Issue>) SearchHitSupport.unwrapSearchHits(page);
	}
	
	@Override
	public Page<Issue> getDocsByTitolDescripcio(String texto, Pageable pageable) {
		
		BoolQueryBuilder boolQuery = new BoolQueryBuilder();
		boolQuery.should(QueryBuilders.matchQuery("titol", texto));
		boolQuery.should(QueryBuilders.matchQuery("descripcio", texto));
		
		FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(boolQuery)
                .boost(0.1f);
		
		NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
				//.withQuery(boolQuery)
				.withQuery(functionScoreQueryBuilder)
				.withHighlightFields(new HighlightBuilder.Field("titol").fragmentSize(50).numOfFragments(10), 
						new HighlightBuilder.Field("descripcio").fragmentSize(200).numOfFragments(10))
				.withPageable(pageable)
				.build();
		
		SearchHits<Issue> searchHits = elasticsearchTemplate.search(searchQuery, Issue.class, IndexCoordinates.of(index));
		SearchPage<Issue> page = SearchHitSupport.searchPageFor(searchHits, searchQuery.getPageable());
		return (Page<Issue>) SearchHitSupport.unwrapSearchHits(page);
	}

}
