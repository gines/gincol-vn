package cat.vn.pr.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cat.vn.pr.config.Constants;
import cat.vn.pr.model.Count;
import cat.vn.pr.model.Doc;
import cat.vn.pr.model.Indice;
import cat.vn.pr.model.Indices;
import cat.vn.pr.model.Issue;
import cat.vn.pr.model.ToSearch;
import cat.vn.pr.service.DocService;
import cat.vn.pr.service.IssueService;

@Controller
public class IndexWebController {

	private static final Logger logger = LoggerFactory.getLogger(IndexWebController.class);
	
	@Autowired
	IssueService issueService;
	@Autowired
	DocService docService;
	
	@Autowired
	private Indices indices;
	
	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String index(final ToSearch tosearch, Model model) {
		logger.info("START index():");
		model.addAttribute("indices", indices);
		if(tosearch!=null && tosearch.getIndice()!=null) {
			model.addAttribute("atributos", indices.getIndiceList().get(Integer.parseInt(tosearch.getIndice())));
			model.addAttribute("indexSelected",tosearch.getIndice());
		} else {
			model.addAttribute("atributos", indices.getIndiceList().get(0));
			model.addAttribute("indexSelected",0);
		}
		return "index";
	}
	
	@RequestMapping(path = "/web/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String getDocs(@RequestBody String payload, Model model) {
		logger.info("START hello():");
		System.out.println(payload);
		return "index";
	}
	
	@RequestMapping(path = "/web/{index}/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String count(@PathVariable("index") String index, Model model) {
		logger.info("START count():");
		Count count = new Count(index,issueService.count());
		model.addAttribute("message",count);
		return "index";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(path = "/web/tosearch", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String tosearch(HttpServletRequest request, final ToSearch tosearch, Model model, @PageableDefault(size = 10) Pageable pageable) {
		logger.info("START tosearch():");

        ToSearch aBuscar = tosearch;
        Indice indice = null;
        String textoBuscado = "", indiceBuscado = "", campoBuscado = "";
		HttpSession miSession= (HttpSession) request.getSession();
		
		if(tosearch.getIndice()!=null) {
			miSession.setAttribute(Constants.NOMBRE_SESSION, tosearch);
			indice = indices.getIndiceList().get(Integer.parseInt(tosearch.getIndice()));
			textoBuscado = tosearch.getTexto();
			indiceBuscado = indices.getIndiceList().get(Integer.parseInt(tosearch.getIndice())).getNombre();
			campoBuscado = indice.getAtributoList().get(Integer.parseInt(tosearch.getCampo())).getNombre();
		}
		
		if(miSession.getAttribute(Constants.NOMBRE_SESSION) != null) {
			aBuscar = (ToSearch) miSession.getAttribute(Constants.NOMBRE_SESSION);
			indice = indices.getIndiceList().get(Integer.parseInt(aBuscar.getIndice()));
			textoBuscado = aBuscar.getTexto();
			indiceBuscado = indices.getIndiceList().get(Integer.parseInt(aBuscar.getIndice())).getNombre();
			campoBuscado = indice.getAtributoList().get(Integer.parseInt(aBuscar.getCampo())).getNombre();
		}
		
		ToSearch tosearchIn = new ToSearch(indiceBuscado, campoBuscado, textoBuscado);
		model.addAttribute("tosearchIn", tosearchIn);
		
		switch (aBuscar.getIndice()) {
			case Constants.INDICE_ISSUE:
				Page<Issue> pageIssue = null;
				switch (aBuscar.getCampo()) {
					case Constants.CAMPO_TITOL:
						pageIssue = issueService.getDocsByTitol(aBuscar.getTexto(), pageable);
						break;
					case Constants.CAMPO_DESCRIPCIO:
						pageIssue = issueService.getDocsByDescripcio(aBuscar.getTexto(), pageable);
						break;
					default:
						pageIssue = issueService.getDocsByTitolDescripcio(aBuscar.getTexto(), pageable);
						break;
				}
				model.addAttribute("page", pageIssue);
				model.addAttribute("totalCount", issueService.count());
				model.addAttribute("searchCount", pageIssue.getTotalElements());
				break;
			case Constants.INDICE_DOCS:
				Page<Doc> pageDocs = docService.getDocsByData(aBuscar.getTexto(), pageable);
				model.addAttribute("page", pageDocs);
				model.addAttribute("totalCount", docService.count());
				model.addAttribute("searchCount", pageDocs.getTotalElements());
				break;
			default:
				break;
		}
        
        return "result";
	}

}
