package cat.vn.pr.model;

public class Count {

	private String indice;
	private long count;

	public Count() {
		super();
	}

	public Count(String indice, long count) {
		super();
		this.indice = indice;
		this.count = count;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

}
