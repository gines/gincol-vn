package cat.vn.pr.model;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import cat.vn.pr.config.YamlPropertySourceFactory;

@Configuration
@EnableConfigurationProperties
@PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:indices.yaml")
@ConfigurationProperties(prefix = "indices")
public class Indice {

	private int id;
	private String nombre;
	private List<Atributo> atributoList;

	public Indice() {
		super();
	}

	public Indice(int id, String nombre, List<Atributo> atributoList) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.atributoList = atributoList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Atributo> getAtributoList() {
		return atributoList;
	}

	public void setAtributoList(List<Atributo> atributoList) {
		this.atributoList = atributoList;
	}
	
}
