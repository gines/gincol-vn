package cat.vn.pr.model;

public class Atributo {

	private int id;
	private String nombre;

	public Atributo() {
		super();
	}

	public Atributo(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
