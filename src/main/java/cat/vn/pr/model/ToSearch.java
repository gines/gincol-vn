package cat.vn.pr.model;

import java.io.Serializable;

public class ToSearch implements Serializable {

	private static final long serialVersionUID = 1L;
	private String indice;
	private String campo;
	private String texto;

	public ToSearch() {
		super();
	}

	public ToSearch(String indice, String campo, String texto) {
		super();
		this.indice = indice;
		this.campo = campo;
		this.texto = texto;
	}

	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}
