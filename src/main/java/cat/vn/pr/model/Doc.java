package cat.vn.pr.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "spain_docs")
public class Doc implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	private Attachment attachment;

	public Doc() {
		super();
	}

	public Doc(String id, Attachment attachment) {
		super();
		this.id = id;
		this.attachment = attachment;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

}
