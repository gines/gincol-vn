package cat.vn.pr.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import cat.vn.pr.config.YamlPropertySourceFactory;

@Configuration
@EnableConfigurationProperties
@PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:indices.yaml")
@ConfigurationProperties(prefix = "indices")
public class Indices {
	private List<Indice> indiceList = new ArrayList<>();

	public Indices() {
	}

	public List<Indice> getIndiceList() {
		return indiceList;
	}

	public void setIndiceList(List<Indice> indiceList) {
		this.indiceList = indiceList;
	}

}
