package cat.vn.pr.model;

public class Attachment {

	private String author;
	private String title;
	private String content;
	private Long content_length;
	private String content_type;
	private String date;

	public Attachment() {
		super();
	}

	public Attachment(String author, String title, String content, Long content_length, String content_type, String date) {
		super();
		this.author = author;
		this.title = title;
		this.content = content;
		this.content_length = content_length;
		this.content_type = content_type;
		this.date = date;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getContent_length() {
		return content_length;
	}

	public void setContent_length(Long content_length) {
		this.content_length = content_length;
	}

	public String getContent_type() {
		return content_type;
	}

	public void setContent_type(String content_type) {
		this.content_type = content_type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
