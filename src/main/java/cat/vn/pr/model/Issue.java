package cat.vn.pr.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "catalan_issue")
public class Issue implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    private String id;
	private String canalEntrada;
	private String causaIncidencia;
	private String clauProjecte;
	private String companyia;
	private String component;
	private String dataCreacio;
	private String dataResolucio;
	private String dataVolcat;
	private String descripcio;
	private String entornRemedy;
	private String estat;
	private String idJira;
	private String idRemedy;
	private String idRequestRemedy;
	private String metodeResolucio;
	private String nomProjecte;
	private String prioritat;
	private String resolucio;
	private String resolutionTier1;
	private String resolutionTier2;
	private String resolutionTier3;
	private String serveiRemedy;
	private String solucioTecnica;
	private String tipus;
	private String tipusTasca;
	private String titol;

	public Issue() {
		super();
	}

	public Issue(String canalEntrada, String causaIncidencia, String clauProjecte, String companyia, String component,
			String dataCreacio, String dataResolucio, String dataVolcat, String descripcio, String entornRemedy, String estat,
			String idJira, String idRemedy, String idRequestRemedy, String metodeResolucio, String nomProjecte,
			String prioritat, String resolucio, String resolutionTier1, String resolutionTier2, String resolutionTier3,
			String serveiRemedy, String solucioTecnica, String tipus, String tipusTasca, String titol) {
		super();
		this.canalEntrada = canalEntrada;
		this.causaIncidencia = causaIncidencia;
		this.clauProjecte = clauProjecte;
		this.companyia = companyia;
		this.component = component;
		this.dataCreacio = dataCreacio;
		this.dataResolucio = dataResolucio;
		this.dataVolcat = dataVolcat;
		this.descripcio = descripcio;
		this.entornRemedy = entornRemedy;
		this.estat = estat;
		this.idJira = idJira;
		this.idRemedy = idRemedy;
		this.idRequestRemedy = idRequestRemedy;
		this.metodeResolucio = metodeResolucio;
		this.nomProjecte = nomProjecte;
		this.prioritat = prioritat;
		this.resolucio = resolucio;
		this.resolutionTier1 = resolutionTier1;
		this.resolutionTier2 = resolutionTier2;
		this.resolutionTier3 = resolutionTier3;
		this.serveiRemedy = serveiRemedy;
		this.solucioTecnica = solucioTecnica;
		this.tipus = tipus;
		this.tipusTasca = tipusTasca;
		this.titol = titol;
	}

	public String getCanalEntrada() {
		return canalEntrada;
	}

	public void setCanalEntrada(String canalEntrada) {
		this.canalEntrada = canalEntrada;
	}

	public String getCausaIncidencia() {
		return causaIncidencia;
	}

	public void setCausaIncidencia(String causaIncidencia) {
		this.causaIncidencia = causaIncidencia;
	}

	public String getClauProjecte() {
		return clauProjecte;
	}

	public void setClauProjecte(String clauProjecte) {
		this.clauProjecte = clauProjecte;
	}

	public String getCompanyia() {
		return companyia;
	}

	public void setCompanyia(String companyia) {
		this.companyia = companyia;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getDataCreacio() {
		return dataCreacio;
	}

	public void setDataCreacio(String dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	public String getDataResolucio() {
		return dataResolucio;
	}

	public void setDataResolucio(String dataResolucio) {
		this.dataResolucio = dataResolucio;
	}

	public String getDataVolcat() {
		return dataVolcat;
	}

	public void setDataVolcat(String dataVolcat) {
		this.dataVolcat = dataVolcat;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public String getEntornRemedy() {
		return entornRemedy;
	}

	public void setEntornRemedy(String entornRemedy) {
		this.entornRemedy = entornRemedy;
	}

	public String getEstat() {
		return estat;
	}

	public void setEstat(String estat) {
		this.estat = estat;
	}

	public String getIdJira() {
		return idJira;
	}

	public void setIdJira(String idJira) {
		this.idJira = idJira;
	}

	public String getIdRemedy() {
		return idRemedy;
	}

	public void setIdRemedy(String idRemedy) {
		this.idRemedy = idRemedy;
	}

	public String getIdRequestRemedy() {
		return idRequestRemedy;
	}

	public void setIdRequestRemedy(String idRequestRemedy) {
		this.idRequestRemedy = idRequestRemedy;
	}

	public String getMetodeResolucio() {
		return metodeResolucio;
	}

	public void setMetodeResolucio(String metodeResolucio) {
		this.metodeResolucio = metodeResolucio;
	}

	public String getNomProjecte() {
		return nomProjecte;
	}

	public void setNomProjecte(String nomProjecte) {
		this.nomProjecte = nomProjecte;
	}

	public String getPrioritat() {
		return prioritat;
	}

	public void setPrioritat(String prioritat) {
		this.prioritat = prioritat;
	}

	public String getResolucio() {
		return resolucio;
	}

	public void setResolucio(String resolucio) {
		this.resolucio = resolucio;
	}

	public String getResolutionTier1() {
		return resolutionTier1;
	}

	public void setResolutionTier1(String resolutionTier1) {
		this.resolutionTier1 = resolutionTier1;
	}

	public String getResolutionTier2() {
		return resolutionTier2;
	}

	public void setResolutionTier2(String resolutionTier2) {
		this.resolutionTier2 = resolutionTier2;
	}

	public String getResolutionTier3() {
		return resolutionTier3;
	}

	public void setResolutionTier3(String resolutionTier3) {
		this.resolutionTier3 = resolutionTier3;
	}

	public String getServeiRemedy() {
		return serveiRemedy;
	}

	public void setServeiRemedy(String serveiRemedy) {
		this.serveiRemedy = serveiRemedy;
	}

	public String getSolucioTecnica() {
		return solucioTecnica;
	}

	public void setSolucioTecnica(String solucioTecnica) {
		this.solucioTecnica = solucioTecnica;
	}

	public String getTipus() {
		return tipus;
	}

	public void setTipus(String tipus) {
		this.tipus = tipus;
	}

	public String getTipusTasca() {
		return tipusTasca;
	}

	public void setTipusTasca(String tipusTasca) {
		this.tipusTasca = tipusTasca;
	}

	public String getTitol() {
		return titol;
	}

	public void setTitol(String titol) {
		this.titol = titol;
	}

}
