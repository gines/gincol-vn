package cat.vn.pr.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import cat.vn.pr.model.Doc;

public interface DocRepository extends ElasticsearchRepository<Doc, String> {
	
	@Query("{\"query\":{\"match\":{\"attachment.content\":\"spring\"}},\"highlight\":{\"pre_tags\":[\"<b>\"],\"post_tags\":[\"<\\/b>\"],\"fields\":{\"attachment.content\":{\"fragment_size\":30,\"number_of_fragments\":10}},\"order\":\"score\"}}")
    Page<Doc> findByDocByDataUsingCustomQuery(String text, Pageable pageable);

	
}
