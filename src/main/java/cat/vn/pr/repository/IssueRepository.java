package cat.vn.pr.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import cat.vn.pr.model.Issue;

public interface IssueRepository extends ElasticsearchRepository<Issue, String> {

	@Query("{\"query_string\":{\"query\":\"?0\",\"default_field\":\"descripcio\"}}")
    Page<Issue> findByDescripcioUsingCustomQuery(String name, Pageable pageable);
	
	@Query("{\"query_string\":{\"query\":\"?0\",\"default_field\":\"titol\"}}")
    Page<Issue> findByTitolUsingCustomQuery(String name, Pageable pageable);
	
	@Query("\"multi_match\":{\"query\":\"?0\",\"fields\":[\"titol\",\"descripcio\"],\"operator\":\"or\"}")
    Page<Issue> findByTitolDescripcioUsingCustomQuery(String name, Pageable pageable);
	
}
